console.log("hello world")
/*
    Create functions which can manipulate our arrays.
*/

let registeredUsers = [

    "James Jeffries",
    "Gunther Smith",
    "Macie West",
    "Michelle Queen",
    "Shane Miguelito",
    "Fernando Dela Cruz",
    "Akiko Yukihime"
];

let friendsList = [];

/*
    
   1. Create a function which will allow us to register into the registeredUsers list.
        - this function should be able to receive a string.
        - determine if the input username already exists in our registeredUsers array.
            -if it is, show an alert window with the following message:
                "Registration failed. Username already exists!"
            -if it is not, add the new username into the registeredUsers array and show an alert:
                "Thank you for registering!"
        - Invoke and register a new user in the browser console.
        - In the browser console, log the registeredUsers array.

*/
    function register(registeredName){
        let registryCheck = registeredUsers.indexOf(registeredName);
        if (registryCheck!=-1){
            alert("Registration Failed Username ''  "+registeredName+"'' already exists!")
        }
        else {
            registeredUsers.push(registeredName);
            alert("thank you for registering")
        }
}
/*
    2. Create a function which will allow us to add a registered user into our friends list.
        - This function should be able to receive a string.
        - Determine if the input username exists in our registeredUsers array.
            - If it is, add the foundUser in our friendList array.
                    -Then show an alert with the following message:
                        - "You have added <registeredUser> as a friend!"
            - If it is not, show an alert window with the following message:
                - "User not found."
        - Invoke the function and add a registered user in your friendsList in the browser console.
        - In the browser console, log the friendsList array in the console.

*/
function addFriend(registeredUser){
        let registryCheck = registeredUsers.indexOf(registeredUser);
        if (registryCheck!=-1){
            
            friendsList.push(registeredUser);
            alert("you have added ''  "+registeredUser+"'' as a friend")
        }
        else {
            
            alert("User not found")
        }
}


/*
    3. Create a function which will allow us to show/display the items in the friendList one by one on our console.
        - If the friendsList is empty show an alert: 
            - "You currently have 0 friends. Add one first."
        - Invoke the function.

*/
  
   function displayFriend(){
   if (friendsList.length==0){
      alert("you currently have no friends add one first");
  }
   else if ((friendsList.length>=1)) {
             for (let i=0; i<friendsList.length;i++){
                 console.log(friendsList[i]);   
        }
}
}


//         if (friendcheck!=0){  
//  function diplayFriendsList(){
//         let friendcheck = friendsList.length;
//         if (friendcheck!=0){
//             ("you currently have no friends add one first")
//         }
//         else {
//             for (let i=0; i<friendsList.length;i++){
//                 console.log(friendsList[i]);      
//         }
// }
//} 
//diplayFriendsList() 
// function test(){
//     console.log("test2")
// }
// test();
/*
    4. Create a function which will display the amount of registered users in your friendsList.
        - If the friendsList is empty show an alert:
            - "You currently have 0 friends. Add one first."
        - If the friendsList is not empty show an alert:
            - "You currently have <numberOfFriends> friends."
        - Invoke the function in the browser console.
*/

function displayNumberOfFriends(){
   if (friendsList.length===0){
      alert("you currently have 0 friends add one first");
  }
   else if ((friendsList.length>=1)) {
         alert("you currently have "+ friendsList.length + " friends");   
        }
    }

/*
    5. Create a function which will delete the last registeredUser you have added in the friendsList.
        - If the friendsList is empty show an alert:
            - "You currently have 0 friends. Add one first."
        - Invoke the function.
        - In the browser console, log the friendsList array.

*/
function deleteFriend(){
   if (friendsList.length===0){
      alert("you currently have 0 friends add one first");
  }
   else {
         let indexToBeRemoved=friendsList.indexOf(friendsList[friendsList.length-1])
          friendsList.splice(indexToBeRemoved,1,);   
        }
    }
    





